"use strict";
import React from 'react';

export default class TweetButton extends React.Component {
  constructor(props) {
    super(props);
    this.suggestion = "Ire al ";
    this.tweet = "https://twitter.com/intent/tweet?text=" + this.suggestion;
  }
  render() {
    return(
      <div>
        <button className="twitter-share-button">
          <a target="_blank"  href={this.tweet + this.props.restOfTweet}>tweet</a>
        </button>
      </div>
    );
  }
}
