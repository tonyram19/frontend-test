"use strict";
import React from 'react';
import Request from 'react-http-request';

import EventCard from './EventCard'

export default class Events extends React.Component {
  constructor(props) {
    super(props);
    this.events = null;
  }

  render() {
    return (
      <Request url='http://localhost:3000/events' method='get' accept='application/json' verbose={true}>
        {
          ({error, result, loading}) => {

            if (loading) {
              return (<div>Loading</div>);
            } else {
              let events = result.body.events.sort(function(a, b) {
                return a.dates[0] > b.dates[0];
              });

              return(
                <div className="mainPanel">
                  <h1 className="mainTitle">Events</h1>
                  {
                    events.map((item, index) => {
                      return(<EventCard key={index} item={item} changeToView={this.props.changeToView}/>);
                    })
                  }
                </div>
              );
            }
          }
        }
      </Request>
    );

  }
}
