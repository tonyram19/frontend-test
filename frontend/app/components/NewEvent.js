"use strict";
import React from 'react';

let request = new XMLHttpRequest();

export default class NewEvent extends React.Component {
  constructor(props){
    super(props);
    this.submit = this.submit.bind(this);
    this.validate = this.validate.bind(this);
    this.state = {"errorMesage": ""};
  }

  validate(object) {
    if (object.event.title == "") {
      return false;
    }
    else if (object.event.eventImage == "") {
      return false;
    }
    else if (object.event.description == "") {
      return false;
    }
    else if (object.event.dates[0] == "") {
      return false;
    }
    else if (object.event.location == "") {
      return false;
    }
    else {
      return true;
    }
  }

  submit() {
    let newEvent = {
      "event": {
        "title": this.refs.eventName.value,
        "eventImage": this.refs.eventImageURL.value,
        "description": this.refs.eventDescription.value,
        "dates": [
          this.refs.eventDate1.value,
          this.refs.eventDate2.value,
          this.refs.eventDate3.value,
          this.refs.eventDate4.value,
          this.refs.eventDate5.value,
          this.refs.eventDate6.value
        ],
        "location": this.refs.eventLocation.value
      }
    };

    if (this.validate(newEvent))
    {
      request.open("POST", "http://localhost:3000/events", true);
      request.setRequestHeader("Content-type", "application/json");
      let changeToMain = ()=>{this.props.changeToMain()};
      request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
          changeToMain();
        }
      };
      request.send(JSON.stringify(newEvent));
    }
    else {
      this.setState({"errorMesage":"You're missing some information!"});
    }

  }
  render() {
    return(
      <div>
        <div className="leftPanelNew">
          <button className="goBackButton" onClick={this.props.changeToMain}>back</button>
          <h1>Create New Event</h1>
          <div ref="inputError" className="inputErrorMessage">{this.state.errorMesage}</div>
          <input ref="eventName" className="eventNameInput" type="text" placeholder="Event Name"/>
          <input ref="eventImageURL" className="eventImageInput" type="text" placeholder="Poster URL"/>
          <input ref="eventLocation" className="eventLocationInput" type="text" placeholder="Event Location"/>
          <textarea ref="eventDescription" className="eventDescriptionInput" rows="10" placeholder="Event Description"/>
        </div>
        <div className="RightPanelNew">
          <input ref="eventDate1" className="eventDateInput" type="text" placeholder="Add At Least One Date"/>
          <input ref="eventDate2" className="eventDateInput" type="text" placeholder="Add Date"/>
          <input ref="eventDate3" className="eventDateInput" type="text" placeholder="Add Date"/>
          <input ref="eventDate4" className="eventDateInput" type="text" placeholder="Add Date"/>
          <input ref="eventDate5" className="eventDateInput" type="text" placeholder="Add Date"/>
          <input ref="eventDate6" className="eventDateInput" type="text" placeholder="Add Date"/>
        </div>
        <button className="saveEventButton" onClick={this.submit}>💾</button>
      </div>

    );
  }
}
