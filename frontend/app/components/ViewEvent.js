"use strict";
import React from 'react';

export default class ViewEvent extends React.Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div>
        <div className="leftPanelView">
          <button className="goBackButton" onClick={this.props.changeToMain}>back</button>
          <h1 className="eventNameView">{this.props.item.title}</h1>
          <div className="eventDescriptionView">{this.props.item.description}</div>
        </div>
        <div className="rightPanelView">
          <img className="eventImageView" src={this.props.item.eventImage}/>
          <div className="eventLocationView">{this.props.item.location}</div>
          {
            this.props.item.dates.map((date, index) => {
              return(<div key={index} className="eventDateView">{date}</div>);
            })
          }
        </div>
      </div>
    );
  }
}
