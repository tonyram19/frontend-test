"use strict";
import React from 'react';

import TweetButton from './TweetButton'

export default class EventCard extends React.Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div className="eventCard">
        <h4 className="date">{this.props.item.dates[0]}</h4>
        <TweetButton restOfTweet={this.props.item.title + " @ " + this.props.item.dates[0]}/>
        <h2 className="eventTitle">{this.props.item.title}</h2>
        <button className="viewButton" onClick={()=>this.props.changeToView(this.props.item)}>View</button>
      </div>
    );
  }
}
