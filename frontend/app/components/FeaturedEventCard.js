"use strict";
import React from 'react';

export default class FeaturedEventCard extends React.Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div className="featuredCard">
        <img className="featuredImage" src={this.props.event.eventImage}/>
        <h3 className="featuredTitle">{this.props.event.title}</h3>
        <div className="featuredDate">{this.props.event.dates[0]}</div>
        <div className="featuredLocation">{this.props.event.location}</div>
        <div className="featuredDescription">{this.props.event.description.substring(0, 90) + "..."}</div>
      </div>
    );
  }
}
