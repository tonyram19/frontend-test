"use strict";
import React from 'react';
import Request from 'react-http-request';

import FeaturedEventCard from './FeaturedEventCard'

export default class FeaturedEvents extends React.Component {
  render() {
    return (
      <Request url='http://localhost:3000/events/featured' method='get' accept='application/json' verbose={true}
      >
        {
          ({error, result, loading}) => {
            if (loading) {
              return (<div>Loading</div>);
            } else {
              return(
                <div className="featuredPanel">
                  <h1>Today's Hilights</h1>
                  {
                    result.body.events.map(function(item, index) {
                      return(<FeaturedEventCard key={index} event={item}/>);
                    })
                  }
                </div>
              );
            }
          }
        }
      </Request>
    );
  }
}
