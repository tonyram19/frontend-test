"use strict";
import React from 'react';
import {render} from 'react-dom';
import Request from 'react-http-request';

import Events from './components/Events'
import FeaturedEvents from './components/FeaturedEvents'
import NewEvent from './components/NewEvent'
import ViewEvent from './components/ViewEvent'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {mode:"main"};
    this.changeToNew = this.changeToNew.bind(this);
    this.changeToMain = this.changeToMain.bind(this);
    this.changeToView = this.changeToView.bind(this);
    this.item = null;
  }

  changeToNew() {
    this.setState({mode:"newEvent"});
  }

  changeToMain() {
    this.setState({mode:"main"});
  }

  changeToView(item) {
    this.setState({mode:"view"});
    this.item = item;
  }

  render() {
    if (this.state.mode == "main") {
      return(
        <div>
          <Events changeToView={this.changeToView}/>
          <FeaturedEvents/>
          <button onClick={this.changeToNew} className="newEventButton">+</button>
        </div>
      );
    }
    else if (this.state.mode == "newEvent") {
      return(
        <div>
          <NewEvent changeToMain={this.changeToMain}/>
        </div>
      );
    }
    else if (this.state.mode = "view")
    {
      return(
        <div>
          <ViewEvent item={this.item} changeToMain={this.changeToMain}/>
        </div>
      );
    }
  }
}

render(<App/>, document.getElementById('app'));
