# Prueba de Ingenious Softworks para candidatos

Para probar la aplicación, primero correr el **backend**.

```
$ cd backend
$ npm install
$ npm start
```

Luego ir a la carpeta de **frontend** e instalar las dependencias

```
$ cd ../frontend
$ npm install
```

Finalmente, montar la carpeta **frontend** en un servidor local. Para mis pruebas usé [Fenix Web Server](http://fenixwebserver.com/), es ligero y sencillo de usar. Al entrar a `http://localhost:PUERTOELEGIDO/frontend/` en el navegador abrirá la aplicación.
